<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function contact(){
        $header = 'To jest nagłówek strony Kontakt';
        $date = '31.08.2020';
        $visited = 4568;
        return view('pages.contact', compact('header', 'date', 'visited'));
    }

    public function about(){

        return view('pages.about');
    }
}
